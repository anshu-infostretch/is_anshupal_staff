# jwt 
from flask import Flask,jsonify, make_response, request
from flask_restful import Resource,Api,abort,reqparse
from flask_mongoengine import MongoEngine
from flask_bcrypt import Bcrypt, generate_password_hash,check_password_hash
import jwt
from functools import wraps

app = Flask(__name__)
api = Api(app)
bcrypt = Bcrypt(app)
# jwt = JWTManager(app)

app.config['JWT_SECRET_KEY'] = "thisismykey"
app.config['MONGODB_SETTINGS'] = {
    'db': 'staff',
    'host' : 'localhost',
    'port' : 27017   
}
db = MongoEngine()
db.init_app(app)

class StaffModel(db.Document):
    _id = db.SequenceField(primary_key=True)
    staff_name = db.StringField()
    staff_email = db.StringField(unique=True)
    staff_role = db.StringField()
    staff_password = db.StringField()

    def hash_password(self):
        self.staff_password = generate_password_hash(self.staff_password).decode('utf8')
    def check_password(self,staff_password):
        return check_password_hash(self.staff_password,staff_password)

class StudentModel(db.Document):
    _id = db.IntField()
    student_name = db.StringField(required=True)
    student_email = db.StringField(required=True)
  
task_insert = reqparse.RequestParser() # flask know that client send data to the server
task_insert.add_argument("student_name", type=str, help="student_name is required",required=True) #add arugments, help=show the error
task_insert.add_argument("student_email", type=str, help="student_email is required",required=True)

task_update = reqparse.RequestParser()
task_update.add_argument("student_name",type=str)
task_update.add_argument("student_email",type=str)


# decorator for verifying the JWT
def admin_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        if 'x-access-tokens' in request.headers:
            token = request.headers['x-access-tokens']
        if not token:
            return jsonify(message = 'Please insert the token !!' )
        try:
            data = jwt.decode(token, app.config['JWT_SECRET_KEY'],algorithms="HS256")
            print(data['staff_role'])
            if data['staff_role'] == "admin" or "superadmin":
                pass
            else:
                return "you can't get the details"
        except:
            return jsonify(message = 'Token is invalid !!' )
        return  f(*args, **kwargs)
    return decorated

def superadmin_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        if 'x-access-tokens' in request.headers:
            token = request.headers['x-access-tokens']
        if not token:
            return jsonify(message = 'Please insert the token !!' ) 
        try:
            data = jwt.decode(token, app.config['JWT_SECRET_KEY'],algorithms="HS256")
            if data['staff_role'] == "superadmin": 
                pass
            else:
                return "you do not have superadmin role!!"
        except:
            return jsonify(message = 'Token is invalid' )
        return  f(*args, **kwargs)
    return decorated

class Student(Resource):
    @admin_required
    def get(self):
        task = StudentModel.objects.all()
        return make_response(jsonify(task))
        
class StudentOne(Resource):
    @superadmin_required
    def get(self,student_id):
        data = StudentModel.objects.get(_id=student_id)
        return jsonify(data)
    
    @superadmin_required
    def post(self,student_id):
        args = task_insert.parse_args() #get data to the server
        data = StudentModel(_id=student_id, student_name=args["student_name"], student_email=args["student_email"]).save()
        return jsonify(data)

    @superadmin_required
    def put(self,student_id):
        args = task_update.parse_args()
        if args['student_name']:
            StudentModel.objects.filter(_id=student_id).update(student_name=args['student_name'])
        if args['student_email']:
            StudentModel.objects.filter(_id=student_id).update(student_email=args['student_email'])
        return "Student id:{} Updated Successfully".format(student_id)

    @superadmin_required
    def delete(self,student_id):
        StudentModel.objects.filter(_id=student_id).delete()
        return "delete"

class Signup(Resource):
    def post(self):
        body = request.get_json()
        staff = StaffModel(**body) # key = value  
        staff.hash_password()
        staff.save()
        return jsonify(staff)
    
class Login(Resource):
    def post(self):
        body = request.get_json()
        staff = StaffModel.objects.get(staff_email = body.get('staff_email'))
        authorized = staff.check_password(body.get('staff_password'))
        if not authorized:
            return{'error':'email or password invaild'}
        else:
            if staff['staff_role'] == 'admin':
                token = jwt.encode({'staff_role': staff['staff_role']}, app.config['JWT_SECRET_KEY'])
                return make_response(jsonify({'message': 'Welcome Admin', 'Token' : token, }))
            elif staff['staff_role'] == 'superadmin':
                token = jwt.encode({'staff_role': staff['staff_role']}, app.config['JWT_SECRET_KEY'])
                return make_response(jsonify({'message': 'Welcome Super Admin', 'Token' : token}))
            else :
                return abort (401, message = "You don't have Admin or Superadmin role")

api.add_resource(Login,'/login')
api.add_resource(Signup,'/signup')
api.add_resource(Student,'/student')
api.add_resource(StudentOne,'/student/<student_id>')

app.run()